package com.digitalmisfits.exchange;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.digitalmisfits.exchange.actor.market.GDAXStreamingActor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Main {

    private static final long ACTOR_SYSTEM_SHUTDOWN_DURATION = 300;
    private static final String ACTOR_SYSTEM_NAME = "MainActorSystem";

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ActorSystem system = ActorSystem.create(ACTOR_SYSTEM_NAME);

    public static void main(String[] args) {
        (new Main()).run();
    }

    private void run() {

        final Session session = initializeCassandraSession();

        final ActorRef actorRef = system.actorOf(GDAXStreamingActor.props(session));
        logger.info("Starting {}", actorRef.path());

        try {
            int read = System.in.read();
        } catch (IOException ignored) {
        } finally {
            system.terminate();
            try {
                Await.result(system.whenTerminated(), Duration.create(ACTOR_SYSTEM_SHUTDOWN_DURATION, TimeUnit.SECONDS));
            } catch (Exception e) {
                LoggerFactory.getLogger(getClass()).info("ActorSystem {} failed to terminate within {} seconds", system.name(), ACTOR_SYSTEM_SHUTDOWN_DURATION);
            }
            logger.info("ActorSystem terminated");
        }
    }

    private Session initializeCassandraSession() {

        final Cluster cluster = Cluster.builder()
                .addContactPoint("127.0.0.1")
                .build();

        return cluster.connect();
    }
}
