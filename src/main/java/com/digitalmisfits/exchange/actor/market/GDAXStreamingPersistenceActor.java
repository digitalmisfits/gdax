package com.digitalmisfits.exchange.actor.market;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;
import com.datastax.driver.core.Session;
import com.digitalmisfits.exchange.actor.market.socket.protocol.inbound.MarketEvent;

public class GDAXStreamingPersistenceActor extends AbstractLoggingActor {

    private final Session session;

    private GDAXStreamingPersistenceActor(final Session session) {
        this.session = session;
    }

    public static Props props(final Session session) {
        return Props.create(GDAXStreamingPersistenceActor.class, () -> new GDAXStreamingPersistenceActor(session));
    }

    @Override
    public Receive createReceive() {

        return receiveBuilder()
                .match(MarketEvent.class, this::onMarketEvent)
                .matchAny(this::unhandled)
                .build();
    }

    private void onMarketEvent(final MarketEvent event) {

    }


}
