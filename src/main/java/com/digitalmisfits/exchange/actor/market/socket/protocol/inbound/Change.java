package com.digitalmisfits.exchange.actor.market.socket.protocol.inbound;

import com.digitalmisfits.exchange.actor.market.socket.protocol.InboundMarketMessage;
import com.digitalmisfits.exchange.actor.market.stream.SequenceOrdered;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("change")
public class Change extends MarketEvent {

    private final long sequence;

    public Change(@JsonProperty("sequence") long sequence) {
        this.sequence = sequence;
    }

    @Override
    public Long getSequence() {
        return this.sequence;
    }
}
