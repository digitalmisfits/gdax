package com.digitalmisfits.exchange.actor.market.socket.protocol.inbound;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

import java.util.List;

public class Channel {

    // e.a. heartbeat, ticker, level2, etc.
    private final String name;

    // e.a. ETH-USD, BTC-ETH, etc.
    private final List<String> productIds;

    @JsonCreator
    public Channel(@JsonProperty("name") String name, @JsonProperty("product_ids") List<String> productIds) {
        this.name = name;
        this.productIds = productIds;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("productIds", productIds)
                .toString();
    }
}
