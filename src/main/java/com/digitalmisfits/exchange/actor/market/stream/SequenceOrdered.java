package com.digitalmisfits.exchange.actor.market.stream;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface SequenceOrdered {

    @JsonProperty
    Long getSequence();
}
