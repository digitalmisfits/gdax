package com.digitalmisfits.exchange.actor.market.socket.protocol.inbound;

import com.digitalmisfits.exchange.actor.market.socket.protocol.InboundMarketMessage;
import com.digitalmisfits.exchange.actor.market.stream.SequenceOrdered;

public abstract class MarketEvent implements InboundMarketMessage, SequenceOrdered {

}
