package com.digitalmisfits.exchange.actor.market.socket.protocol.inbound;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("received")
public class Received extends MarketEvent {

    private final long sequence;

    public Received(@JsonProperty("sequence") long sequence) {
        this.sequence = sequence;
    }

    @Override
    public Long getSequence() {
        return this.sequence;
    }
}
