package com.digitalmisfits.exchange.actor.market.socket.protocol.inbound;

import com.digitalmisfits.exchange.actor.market.socket.protocol.InboundMarketMessage;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("l2update")
public class Level2Update implements InboundMarketMessage {

}
