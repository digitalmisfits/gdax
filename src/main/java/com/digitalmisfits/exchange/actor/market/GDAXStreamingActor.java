package com.digitalmisfits.exchange.actor.market;

import akka.actor.*;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.datastax.driver.core.Session;
import com.digitalmisfits.exchange.actor.market.protocol.internal.StreamingConnectionActive;
import com.digitalmisfits.exchange.actor.market.socket.protocol.inbound.*;
import com.digitalmisfits.exchange.actor.market.socket.protocol.outbound.Subscribe;
import com.google.common.collect.Lists;
import scala.concurrent.duration.Duration;

import java.util.List;
import java.util.Optional;

public class GDAXStreamingActor extends AbstractActorWithStash {

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    private final Session session;

    private Optional<ActorRef> connection = Optional.empty();
    private Optional<ActorRef> persistence = Optional.empty();

    private GDAXStreamingActor(final Session session) {
        this.session = session;
    }

    public static Props props(final Session session) {
        return Props.create(GDAXStreamingActor.class, () -> new GDAXStreamingActor(session));
    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return new OneForOneStrategy(0, Duration.Zero(), cause -> {
            self().tell(new StreamingConnectionFailure(cause), getSelf());
            return SupervisorStrategy.stop();
        });
    }

    @Override
    public void preStart() {

        final ActorRef persistenceActorRef
                = getContext().actorOf(GDAXStreamingPersistenceActor.props(session), "persistence");
        getContext().watch(persistenceActorRef);
        log.info("Starting persistence actor {}", persistenceActorRef);

        persistence = Optional.of(persistenceActorRef);


        final ActorRef connectionActorRef
                = getContext().actorOf(GDAXStreamingConnectionActor.props(), "connection");
        getContext().watch(connectionActorRef);
        log.info("Starting connection actor {}", connectionActorRef);

        connection = Optional.of(connectionActorRef);
    }

    @Override
    public Receive createReceive() {

        return receiveBuilder()
                .match(StreamingConnectionActive.class, this::onStreamingConnectionActive)
                .match(StreamingConnectionFailure.class, this::onStreamingConnectionFailure)
                .match(Terminated.class, this::onTerminated)
                .build();
    }

    private void onTerminated(Terminated terminated) {
        log.debug("Actor {} terminated.", terminated.actor());
    }

    private void onStreamingConnectionActive(final StreamingConnectionActive active) {

        subscribeToChannels();

        final Receive receive = receiveBuilder()
                .match(Subscriptions.class, this::onSubscriptions)
                .match(Heartbeat.class, this::onHeartbeat)
                .match(Ticker.class, this::onTicker)
                .match(Level2Snapshot.class, this::onLevel2Snapshot)
                .match(Level2Update.class, this::onLevel2Update)
                .match(ProtocolError.class, this::onProtocolError)
                .match(MarketEvent.class, this::onMarketEvent)
                .match(StreamingConnectionFailure.class, this::onStreamingConnectionFailure)
                .match(Terminated.class, this::onTerminated)
                .matchAny(this::unhandled)
                .build();

        getContext().become(receive);
    }

    private void onStreamingConnectionFailure(final StreamingConnectionFailure failure) {
        log.error(failure.cause, "StreamingConnectionFailure");
    }

    private void onProtocolError(final ProtocolError e) {

    }

    private void subscribeToChannels() {

        final List<String> channels = Lists.newArrayList(
                "heartbeat", "full");

        final List<String> products = Lists.newArrayList(
                "BTC-USD");

        out(new Subscribe(products, channels), self());
    }

    private void out(final Object subscribe, final ActorRef self) {

        connection.orElseThrow(IllegalStateException::new)
                .tell(subscribe, self);
    }

    private void onSubscriptions(final Subscriptions subscriptions) {
        log.info(subscriptions.toString());
    }

    private void onTicker(final Ticker ticker) {

    }

    private void onHeartbeat(final Heartbeat heartbeat) {

    }

    private void onLevel2Snapshot(final Level2Snapshot snapshot) {

    }

    private void onLevel2Update(final Level2Update update) {

    }

    private void onMarketEvent(final MarketEvent event) {

        persistence.orElseThrow(IllegalStateException::new)
                .tell(event, self());
    }

    private static class StreamingConnectionFailure {
        final Throwable cause;

        StreamingConnectionFailure(Throwable cause) {
            this.cause = cause;
        }
    }
}
