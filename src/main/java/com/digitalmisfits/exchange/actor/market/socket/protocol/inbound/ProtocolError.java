package com.digitalmisfits.exchange.actor.market.socket.protocol.inbound;

import com.digitalmisfits.exchange.actor.market.socket.protocol.InboundMarketMessage;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.google.common.base.MoreObjects;

@JsonTypeName("error")
public class ProtocolError implements InboundMarketMessage {

    private final String message;
    private final String original;

    @JsonCreator
    public ProtocolError(@JsonProperty("message") String message, @JsonProperty("original") String original) {
        this.message = message;
        this.original = original;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("message", message)
                .add("original", original)
                .toString();
    }
}
