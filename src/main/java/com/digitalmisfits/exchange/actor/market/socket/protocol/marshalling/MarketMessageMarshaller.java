package com.digitalmisfits.exchange.actor.market.socket.protocol.marshalling;


import com.digitalmisfits.exchange.actor.market.socket.WireMessage.Outbound;
import com.digitalmisfits.exchange.actor.market.socket.protocol.InboundMarketMessage;
import com.digitalmisfits.exchange.actor.market.socket.protocol.OutboundMarketMessage;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.guava.GuavaModule;

import java.io.IOException;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;

public class MarketMessageMarshaller {

    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.setSerializationInclusion(Include.NON_NULL);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        mapper.setVisibility(PropertyAccessor.IS_GETTER, Visibility.NONE);
        mapper.setVisibility(PropertyAccessor.GETTER, Visibility.NONE);
        mapper.registerModule(new GuavaModule());
    }

    /**
     * Unmarshall a JSON(rfc4627) {@link String} into a {@link InboundMarketMessage}
     *
     * @param json a JSON(rfc4627) encoded {@link String}
     * @return a {@link InboundMarketMessage}
     * @throws IOException An {@link IOException} is thrown if the JSON(rfc4627) {@link String} cannot be unmarshalled to a {@link InboundMarketMessage}
     */
    public static InboundMarketMessage unmarshall(final String json) throws IOException {

        return mapper.readValue(json, new TypeReference<InboundMarketMessage>() {
            // No Implementation
        });
    }

    public static Outbound marshall(final OutboundMarketMessage message) throws JsonProcessingException {

        return new Outbound(mapper.writeValueAsBytes(message));
    }
}