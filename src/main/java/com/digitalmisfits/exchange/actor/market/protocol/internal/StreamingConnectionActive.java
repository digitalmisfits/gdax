package com.digitalmisfits.exchange.actor.market.protocol.internal;

import akka.actor.ActorRef;

public class StreamingConnectionActive {

    public final ActorRef source;

    public StreamingConnectionActive(final ActorRef source) {
        this.source = source;
    }
}
