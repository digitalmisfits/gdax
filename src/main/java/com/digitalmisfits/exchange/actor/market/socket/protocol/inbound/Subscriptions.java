package com.digitalmisfits.exchange.actor.market.socket.protocol.inbound;

import com.digitalmisfits.exchange.actor.market.socket.protocol.InboundMarketMessage;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.google.common.base.MoreObjects;

import java.util.List;

@JsonTypeName("subscriptions")
public class Subscriptions implements InboundMarketMessage {

    public final List<Channel> channels;

    @JsonCreator
    public Subscriptions(@JsonProperty("channels") List<Channel> channels) {
        this.channels = channels;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("channels", channels)
                .toString();
    }
}
