package com.digitalmisfits.exchange.actor.market;

import akka.NotUsed;
import akka.actor.AbstractActorWithStash;
import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.ws.Message;
import akka.http.javadsl.model.ws.TextMessage;
import akka.http.javadsl.model.ws.WebSocketRequest;
import akka.http.javadsl.model.ws.WebSocketUpgradeResponse;
import akka.stream.ActorMaterializer;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import com.digitalmisfits.exchange.actor.market.protocol.internal.StreamingConnectionActive;
import com.digitalmisfits.exchange.actor.market.socket.WireMessage.Inbound;
import com.digitalmisfits.exchange.actor.market.socket.WireMessage.Outbound;
import com.digitalmisfits.exchange.actor.market.socket.protocol.InboundMarketMessage;
import com.digitalmisfits.exchange.actor.market.socket.protocol.OutboundMarketMessage;
import com.digitalmisfits.exchange.actor.market.socket.protocol.inbound.MarketEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import scala.concurrent.duration.FiniteDuration;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

import static com.digitalmisfits.exchange.actor.market.socket.protocol.marshalling.MarketMessageMarshaller.marshall;
import static com.digitalmisfits.exchange.actor.market.socket.protocol.marshalling.MarketMessageMarshaller.unmarshall;
import static java.util.concurrent.CompletableFuture.completedFuture;

public class GDAXStreamingConnectionActor extends AbstractActorWithStash {

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    private final ActorMaterializer materializer = ActorMaterializer.create(getContext().system());
    private final Http http = Http.get(getContext().system());

    private Optional<ActorRef> source = Optional.empty();

    public static Props props() {
        return Props.create(GDAXStreamingConnectionActor.class, GDAXStreamingConnectionActor::new);
    }

    @Override
    public void preStart() {

        final ActorRef self = getSelf();

        final Sink<Message, NotUsed> sink = Flow.<Message>create()
                .map((Message inbound) -> {
                    final TextMessage message = inbound.asTextMessage();
                    if (message.isStrict()) {
                        return completedFuture(new Inbound(message.getStrictText().getBytes()));
                    } else {
                        final CompletionStage<String> flattened = message.getStreamedText()
                                .limit(65536)
                                .completionTimeout(FiniteDuration.create(10, TimeUnit.SECONDS))
                                .runFold("", String::concat, materializer);
                        return flattened.thenApply(s -> completedFuture(new Inbound(s.getBytes())));
                    }
                })
                .mapAsync(3, inboundCompletionStage -> inboundCompletionStage)
                .to(Sink.actorRef(self, PoisonPill.getInstance()));

        final Source<Message, NotUsed> source = Source.<Outbound>actorRef(5, OverflowStrategy.fail())
                .map((outbound) -> (Message) TextMessage.create(new String(outbound.message, "utf-8")))
                .mapMaterializedValue(destinationRef -> {
                    self.tell(new StreamingConnectionActive(destinationRef), ActorRef.noSender());
                    return NotUsed.getInstance();
                });

        final Flow<Message, Message, CompletionStage<WebSocketUpgradeResponse>> webSocketFlow =
                http.webSocketClientFlow(WebSocketRequest.create("wss://ws-feed.gdax.com"));

        source.viaMat(webSocketFlow, Keep.left())
                .toMat(sink, Keep.left())
                .run(materializer);
    }

    @Override
    public Receive createReceive() {

        return receiveBuilder()
                .match(StreamingConnectionActive.class, this::onMarketStreamConnected)
                .matchAny(this::stashInboundMessage)
                .build();
    }

    private void onMarketStreamConnected(final StreamingConnectionActive connected) {

        source = Optional.of(connected.source);

        getContext().getParent()
                .tell(connected, self());

        final Receive receive = receiveBuilder()
                .match(Inbound.class, this::onInbound)
                .match(OutboundMarketMessage.class, this::onOutboundMarketMessage)
                .matchAny(this::unhandled)
                .build();

        getContext().become(receive);
    }

    private void onInbound(Inbound inbound) throws UnsupportedEncodingException {
        log.debug("Inbound {}", new String(inbound.message, "utf-8"));

        final InboundMarketMessage message;
        try {
            message = unmarshall(new String(inbound.message, "utf-8"));
        } catch (IOException e) {
            throw new UnmarshallingFailedException("Unmarshalling of inbound message failed", e);
        }

        getContext().getParent()
                .tell(message, getSelf());
    }

    private void onOutboundMarketMessage(OutboundMarketMessage message) throws UnsupportedEncodingException {
        log.debug("Outbound {}", message);

        final Outbound outbound;
        try {
            outbound = marshall(message);
        } catch (JsonProcessingException e) {
            throw new MarshallingFailedException("Unmarshalling of inbound message failed", e);
        }

        log.debug("Encoded {}", new String(outbound.message, "utf-8"));

        source.orElseThrow(() -> new IllegalStateException("Stream source unavailable."))
                .tell(outbound, self());
    }

    /**
     * Stash {@link Inbound}, ignore any other messages
     *
     * @param msg the message
     */
    private void stashInboundMessage(Object msg) {

        if (msg instanceof Inbound) {
            log.debug("Stashing InboundMessage");
            stash();
        } else {
            log.debug("Not stashing unexpected message {}", msg.getClass());
        }
    }

    @Override
    public void postStop() {
        log.info("Actor stopped");
    }

    public static class UnmarshallingFailedException extends RuntimeException {

        UnmarshallingFailedException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static class MarshallingFailedException extends RuntimeException {

        MarshallingFailedException(String message, Throwable cause) {
            super(message, cause);
        }
    }

}
