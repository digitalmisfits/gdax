package com.digitalmisfits.exchange.actor.market.socket.protocol.inbound;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("open")
public class Open extends MarketEvent {

    private final long sequence;

    public Open(@JsonProperty("sequence") long sequence) {
        this.sequence = sequence;
    }

    @Override
    public Long getSequence() {
        return this.sequence;
    }
}
