package com.digitalmisfits.exchange.actor.market.socket.protocol;

import com.digitalmisfits.exchange.actor.market.socket.protocol.inbound.*;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(Subscriptions.class),
        @JsonSubTypes.Type(ProtocolError.class),
        @JsonSubTypes.Type(Heartbeat.class),
        @JsonSubTypes.Type(Ticker.class),
        @JsonSubTypes.Type(Level2Snapshot.class),
        @JsonSubTypes.Type(Level2Update.class),
        @JsonSubTypes.Type(Received.class),
        @JsonSubTypes.Type(Open.class),
        @JsonSubTypes.Type(Done.class),
        @JsonSubTypes.Type(Match.class),
        @JsonSubTypes.Type(Activate.class)
})
public interface InboundMarketMessage {

}
