package com.digitalmisfits.exchange.actor.market.socket;

public class WireMessage {

    static class Unidirectional {
        public final byte[] message;

        Unidirectional(byte[] message) {
            this.message = message;
        }
    }

    public static class Inbound extends Unidirectional {
        public Inbound(byte[] message) {
            super(message);
        }
    }

    public static class Outbound extends Unidirectional {
        public Outbound(byte[] message) {
            super(message);
        }
    }
}
