package com.digitalmisfits.exchange.actor.market.socket.protocol.inbound;

import com.digitalmisfits.exchange.actor.market.socket.protocol.InboundMarketMessage;
import com.digitalmisfits.exchange.actor.market.stream.SequenceOrdered;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("heartbeat")
public class Heartbeat implements InboundMarketMessage, SequenceOrdered {

    private final long sequence;

    @JsonCreator
    public Heartbeat(@JsonProperty("sequence") long sequence) {
        this.sequence = sequence;
    }

    @Override
    public Long getSequence() {
        return sequence;
    }
}
