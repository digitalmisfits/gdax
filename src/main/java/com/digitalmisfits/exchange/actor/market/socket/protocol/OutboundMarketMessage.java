package com.digitalmisfits.exchange.actor.market.socket.protocol;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface OutboundMarketMessage {

    @JsonProperty
    String getType();
}
