package com.digitalmisfits.exchange.actor.market.socket.protocol.inbound;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("match")
public class Match extends MarketEvent {

    private final long sequence;

    public Match(@JsonProperty("sequence") long sequence) {
        this.sequence = sequence;
    }

    @Override
    public Long getSequence() {
        return this.sequence;
    }
}
