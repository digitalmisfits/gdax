package com.digitalmisfits.exchange.actor.market.socket.protocol.outbound;

import com.digitalmisfits.exchange.actor.market.socket.protocol.OutboundMarketMessage;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.google.common.base.MoreObjects;

import java.util.List;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Subscribe implements OutboundMarketMessage {

    @JsonProperty
    final List<String> productIds;

    @JsonProperty
    final List<String> channels;

    public Subscribe(final List<String> productIds, final List<String> channels) {
        this.productIds = productIds;
        this.channels = channels;
    }

    @Override
    public String getType() {
        return "subscribe";
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("productIds", productIds)
                .add("channels", channels)
                .toString();
    }
}
