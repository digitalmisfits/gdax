package com.digitalmisfits.exchange.extension;

import akka.actor.AbstractExtensionId;
import akka.actor.ExtendedActorSystem;
import akka.actor.ExtensionIdProvider;

public class Settings extends AbstractExtensionId<SettingsImpl> implements ExtensionIdProvider {

    public final static Settings SettingsProvider = new Settings();

    @Override
    public Settings lookup() {
        return Settings.SettingsProvider;
    }

    @Override
    public SettingsImpl createExtension(ExtendedActorSystem system) {
        return new SettingsImpl(system.settings().config());
    }
}
